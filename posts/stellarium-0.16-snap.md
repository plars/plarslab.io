.. title: Stellarium 0.16 is released (and as a snap)
.. slug: stellarium-0.16-snap.md
.. date: 2017-06-22 10:37:05 UTC-05:00
.. tags: snap snapcraft stellarium astronomy ubuntu
.. category: 
.. link: 
.. description: 
.. type: text

I noticed today that [Stellarium](http://www.stellarium.org) 0.16 has been
released.  It's not official or anything, but I've been maintaining a
snap for stellarium for a while now, so I thought this would be a great
time to update it and make sure it's available for others who use it as a
snap.  I spent a bit of time testing it, added one additonal dependency
that was needed at build time, but otherwise all it took was changing the
version number and updating the branch I point at to the .16 branch. I
have build.snapcraft.io monitoring my snapcraft branch for changes, and it
automaticaly built a new version.

So if you are interested in astronomy, a stellarium user, or even if you just
want to get a sneak peak at the upcoming solar eclipse without waiting or
standing out in the sun, check it out!  All you need to do is run:

```bash
$ sudo snap install stellarium-plars
```

Or if you've already installed it, you probably have the new version already.

Enjoy!
