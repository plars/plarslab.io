.. title: Automated Device Recovery (part 2)
.. slug: automated-device-recovery-part-2
.. date: 2015-09-01 09:24:56 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Following my previous post about phone provisioning and recovery, new devices
required me to rethink the approach. The breakout boards were great, and
mostly worked well, but other devices are not so easy due to custom
connectors. Also, disassembling the device means we lose the connection to the
antenna, and is time consuming.  So the approach from here on out is to
create something that presses the buttons mechanically.  Using cheap hobby
servos and a 3d printer, I created some simple frames that hold the phone and
allow the servos to press the buttons as needed. Servos are controlled via a
small flask app with a simple api to tell it which pin to press/unpress
running on a raspberry pi with a servo hat. There's a neat little servo
mount that I hacked together in the design that holds them only by friction.
This way if they ever need to be replaced, it's really simple to do.

The frames have to be modified a bit for different phones, but are mostly
the same. I have the
[openscad source available on github](https://github.com/plars/robothands).


Here's a picture of the completed prototype.

![phone automation frame](/images/mx4-frame.jpg)

Using this method, the buttons can be automatically held down in the
right sequence to force the device into fastboot mode so that it can
accept a new image - regardless of the state it was in before.

