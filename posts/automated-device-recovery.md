.. title: Automated Device Recovery
.. slug: automated-device-recovery
.. date: 2015-07-02 09:24:56 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
On the Continuous Integration Team at Canonical, I had the honor of
getting to work with the very first [Ubuntu phone](http://www.ubuntu.com/phone>) to ship, the BQ Aquaris. When providing testing infrastructure for these devices, we
really wanted it to be solid, reliable, and reproducible. This meant
installing new images before we know if they might be broken, and even
installing potentially unstable code before it lands in production. In
short, there's no easy way to know in advance whether the thing we're
about to install will work, break, or have a catastrophic failure and leave
the device unable to boot. No matter what happens, we want the device to
be stable and ready to test the next thing we through at it, without
someone having to sit in a lab and manually recover bricked phones all
day. In reality, things usually go quite well thanks to careful review
from developers before trying to land code. But you should always plan
for the worst.

The good news is that these phones were all built with the ability to
load new images from fastboot mode, regardless of whether the previous
image we loaded on it was stable or broken. Getting to fastboot mode on
the BQ Aquaris requires holding down the power button, volume up, and
volume down for about 15 seconds, followed by volume down + up only for
5 seconds. This effectively forces the device to turn off, and turn back
on with the necessary button combination to force it into fastboot. Once
in that mode, a host system can run ubuntu-device-flash to install a new
image.

There are a few different options for instrumenting this. On the Nexus
4, which we used as a reference platform, we accomplished this by
soldering tiny wires to the contacts for the buttons. This worked, but
turned out to be unreliable. Those connections are very small and easy
to break the connection. It also requires enough disassembly of each
device that doing more than a handful of them is very time consuming.

Disassembly of the Aquaris revealed a nice surprise.

<img src="/images/krillin-pins.jpg" width="30%">

With a little probing, I was able to figure out the following pinouts
from that one little header:

```bash
5+6 = power
2+3 = volume down
1+4 = volume up
```

I found a neat place called proto-advantage that sells breakout boards for
exactly this sort of thing. In fact, they will even order the connector, and do all the assembly for an additional $8.00.  All we need to do is pop the back off the phone, connect the wires, and we're done.  Here's a breakdown of the parts:


| Part | Price |
| ---- | ----- |
| [Molex FFC / FPC Jumper Cables](http://www.mouser.com/ProductDetail/Molex/15166-0056/?qs=%2fha2pyFaduhlpqqIT4%2fG8bu8xxoAINkpfbqFvvRUz%2fIHTnvkCuCBcA%3d%3d>) | $1.82 |
| [FPC/FFC SMT Connector (0.5 mm pitch, 10 pin or less) DIP Adapter](http://www.proto-advantage.com/store/product_info.php?products_id=3400010>) | $3.69 |
| Digikey part #HFT106CT-ND | $2.05 |
| Soldering and assembly | $8.00 |



Aside from that, all we need are some jumper wires to connect from the breakout board to the relays, and the rest was just software to drive the process of detecting if we have an unreachable phone, and driving the relays to force it into fastboot if we do. From there, it's a simple matter to run ubuntu-device-flash and get back to a stable image, and ready for the next round of tests.

Here are some additional pictures of a closeup of the breakout board and
a picture with it connected to the phone:

<img src="/images/krillin-breakout-closeup.jpg" width="30%">
<img src="/images/breakout-krillin.jpg " width="30%">

